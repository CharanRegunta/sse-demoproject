package com.howtodoinjava.springasyncexample.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.jayway.jsonpath.JsonPath;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.howtodoinjava.springasyncexample.web.model.DataSet;
import com.howtodoinjava.springasyncexample.web.service.DataSetService;

@RestController
public class DataSetController {

    @GetMapping("/{sessionId}/sample-guide")
    public SseEmitter sendEventResponse(@PathVariable("sessionId") String sessionId) throws IOException {
        SseEmitter emitter = new SseEmitter();
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            try {
                while (true) {
                    String receiveEvents = receiveEventsFromEvent();
                    String eventType = JsonPath.parse(receiveEvents).read("eventType").toString();
                    if ("doSomething".equals(eventType)) {
                        randomDelay();
                        JSONObject json = new JSONObject();
                        json.put("sessionId", sessionId);
                        json.put("message", "Welcome to Embibe-Guide");
                        emitter.send(json.toString());

                    }
                    else if ("endSession".equals(eventType)) {
                        break;
                    }
                    else{
                    }
                }
                emitter.complete();
            } catch (IOException | JSONException e) {
                emitter.completeWithError(e);
            }
        });
        executor.shutdown();
        return emitter;
    }

    private String receiveEventsFromEvent() throws JSONException {
        JSONObject eventResponse = new JSONObject();
        eventResponse.put("eventType", "doSomething");
        return eventResponse.toString();
    }


    private void randomDelay() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
